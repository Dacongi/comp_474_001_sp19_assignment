import unittest

from item import *


# define a mock database that will satisfy the above class.
class MockDB:

    def __init__(self):
        """
        Initialize an empty dictionary to hold a set of items ids and item.
        Storage is by id (key) and  item class (value). This is makes lookup
		by id very quick.
        """
        self.items = {}

    def addItem(self, id, item):
        """
        Add an item to the db. Assert if a key already exists with thisid. No need to be nice with the Mock DB.
        """

        itemAlreadyExists = id in self.items
        if itemAlreadyExists:
            raise Exception("Error: item name already exists.")

        self.items[id] = item

    def getItem(self, id):
        """
        get an item ftom db with a id
        """
        if id in self.items.keys():
            return self.items[id]
        else:
            return None


# create a little unit test to show how to connect the
# MockDB with the dependent class that we want to actually test.
## Python unittest test suite.
class TestMockDb(unittest.TestCase):

    def setUp(self):
        """
          Setup internal database needed for the test cases.
          We can create a single mock-db object here if necessary (e.g.,
          if it's very expensive or cumbersome to construct) or you need
          a singleton object. You may need unique db's for certain test
          case though. In this instance, the db is very lite so you can
          create several if you'd like.
        """

        self.mock_db = MockDB()
        item1 = Item("apple", 0.25)
        item2 = Item("chair", 10.31)
        item3 = Item("Pear", 0.45)
        item4 = Item("Table", 39.99)
        # Add some items to the db.
        self.mock_db.addItem(1,item1)
        self.mock_db.addItem(2,item2)
        self.mock_db.addItem(3,item3)
        self.mock_db.addItem(4,item4)

    def tearDown(self):
        """
        Clean up all internal data created in setUp().
        """
        self.mock_db.items.clear()

    def test_lookup_item_exists(self):
        """
        Check if you can look up an item that exists in the db.
        """
        item = Item("apple", 0.25)
        item1 = self.mock_db.getItem(1)
        self.assertIsNotNone(item1)
        self.assertEqual(item1.getPrice(), item1.getPrice())

    def test_lookup_item_does_not_exists(self):
        """
        Check that you catch a failure when trying to query an item
        that doens't exist in the db.
        """

        self.assertIsNone(self.mock_db.getItem(6))


if __name__ == '__main__':
    # unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(TestMockDb)
    unittest.TextTestRunner(verbosity=2).run(suite)