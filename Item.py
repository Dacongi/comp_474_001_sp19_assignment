import unittest



# Define an item  that needs to interface with the mock db object.
class Item:
    def __init__(self, name, price):
        self.price = price
        self.name = name

    def getPrice(self):
        return self.price


class TestItem(unittest.TestCase):
    def setUp(self):
        """
          Setup some Item for test cases.
        """
        self.item1 = Item("apple", 0.25)


    def tearDown(self):
        """
        Clean up all internal data created in setUp().
        """
        del self.item1

    def test_lookup_item_exists(self):
        """
        Check if you can look up an item that exists in the db.
        """

        price = self.item1.getPrice()
        self.assertIsNotNone(price)
        self.assertEqual(price, 0.25)


if __name__ == '__main__':
    # unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(TestClassNeedsMockObject)
    unittest.TextTestRunner(verbosity=2).run(suite)