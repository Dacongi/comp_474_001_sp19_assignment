import unittest

from mockdb import MockDB
from item import Item

import decimal


class ShoppingCart:
    def __init__(self, club, tax_exempt, db):
        self.club = club
        self.tax_exempt = tax_exempt
        self.db = db

    def calculatePurchasePrice(self, product_ids, customer):
        discount = 0
        tax = 0.045

        if len(product_ids) > 50:
            return "Error"
        if 10 <= len(product_ids) <= 50:
            discount += 0.1
        elif 5 < len(product_ids) < 10:
            discount += 0.05
        if customer in self.club:
            discount += 0.1
        if customer in self.tax_exempt:
            tax = 0
        net_price = 0
        for id in product_ids:
            net_price += self.db.getItem(id).getPrice()

        return self.to_String(net_price * (1 - discount) * (1 + tax))

    def to_String(self, value):
        value = '{:.2f}'.format(decimal.Decimal(str(value)))
        decimal.getcontext().rounding = decimal.ROUND_HALF_UP
        return "${}".format(value)


class TestShoppingCart(unittest.TestCase):
    def setUp(self):
        """
          Setup some Item for test cases.
        """
        # create some item
        self.item1 = Item("Apple", 1.00)
        self.item2 = Item("CD", 10.00)
        self.item3 = Item("Table", 20.00)
        self.item4 = Item("Chair", 40.00)
        self.item5 = Item("Desk", 50.00)
        self.item6 = Item("", 100)
        self.item7 = Item("", 150)
        self.item8 = Item("", 200)
        self.item9 = Item("", 400)
        self.item10 = Item("", 25.00)
        self.item11 = Item("", 1.0125)
        self.item12 = Item("", 1.0625)
        self.item13 = Item("", 1.0250)
        self.item14 = Item("", 1.0249)
        self.item15 = Item("", 1.6250)
        self.item16 = Item("", 0.9900)
        self.item17 = Item("", 1.0000)
        # get item into database
        self.db = MockDB()
        self.db.addItem(1, self.item1)
        self.db.addItem(2, self.item2)
        self.db.addItem(3, self.item3)
        self.db.addItem(4, self.item4)
        self.db.addItem(5, self.item5)
        self.db.addItem(6, self.item6)
        self.db.addItem(7, self.item7)
        self.db.addItem(8, self.item8)
        self.db.addItem(9, self.item9)
        self.db.addItem(10, self.item10)
        self.db.addItem(11, self.item11)
        self.db.addItem(12, self.item12)
        self.db.addItem(13, self.item13)
        self.db.addItem(14, self.item14)
        self.db.addItem(15, self.item15)
        self.db.addItem(16, self.item16)
        self.db.addItem(17, self.item17)

        # init shopping cart with club_member, tax_exempt_member,
        self.shopping_cart = ShoppingCart(["Alice", "Bob"], ["Alice", "Kevin"], self.db)
        # create some product_ids
        self.product_ids1 = [1 for i in range(100)]  # 100 items
        self.product_ids2 = [3 for i in range(10)] + [4 for i in range(20)]  # 30 items
        self.product_ids3 = [6] + [7 for i in range(6)]  # 7 items
        self.product_ids4 = [8] + [9, 9]  # 3 items
        self.product_ids5 = [3 for i in range(45)] + [10 for i in range(4)]  # 49 items
        self.product_ids6 = [3 for i in range(50)]  # 50 items
        self.product_ids7 = [3 for i in range(51)]  # 51 items
        self.product_ids8 = [6 for i in range(10)]  # 10 items
        self.product_ids9 = [8 for i in range(5)]  # 5 items
        self.product_ids10 = [8 for i in range(4)] + [6, 6]
        self.product_ids11 = [11] # 1 items   $1.0125
        self.product_ids12 = [12] # 1 items   $ 1.0625
        self.product_ids13 = [13] # 1 items  $1.0250
        self.product_ids14 = [14] # 1 items $ 1.0249
        self.product_ids15 = [15] # 1 items $ 1.6250
        self.product_ids16 = [16] # 1 items $ 0.9900
        self.product_ids17 = [17] # 1 items $ 1.00

    def tearDown(self):
        """
        Clean up all internal data created in setUp().
        """
        pass

    def test_items_count_greater_than_100(self):
        """
        1. Check item count greater than 50 whether will return error.
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids1, "eve"), "Error")

    def test_items_count_greater_than_10(self):
        """
        2. Check item count greater than 10 and less than 50, customer without tax_exempt and not club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids2, "eve"), "$940.50")

    def test_items_count_greater_than_5(self):
        """
        3. Check item count greater than 5 and less than 10, customer without tax_exempt and not club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids3, "eve"), "$992.75")

    def test_items_count_less_than_5(self):
        """
        4. Check item count less than 5, customer without tax_exempt and not club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids4, "eve"), "$1045.00")

    def test_items_count_greater_than_10_and_customer_in_club_and_tax_exempt(self):
        """
        5. Check item count greater than 10 and less than 50, customer is  club memeber with tax_exempt
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids2, "Alice"), "$800.00")

    def test_items_count_less_than_5_and_customer_in_club_without_tax_exempt(self):
        """
        6. Check item count less than 5, customer  is club memeber without tax_exempt
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids4, "Bob"), "$940.50")

    def test_items_count_greater_than_10_and_customer_in_club_without_tax_exempt(self):
        """
        7. Check item count greater than 10 and less than 50, customer is club memeber without tax_exempt
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids2, "Bob"), "$836.00")

    def test_items_count_greater_than_10_and_customer_with_tax_exempt_not_in_club(self):
        """
        8. Check item count greater than 10 and less than 50, customer  with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids5, "Kevin"), "$900.00")

    def test_items_count_equal_50_and_customer_with_tax_exempt(self):
        """
        9. Check item count equal with 50, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids6, "Kevin"), "$900.00")

    def test_items_count_greater_than_50_and_customer_with_tax_exempt(self):
        """
        10. Check item count greater than 50, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids7, "Kevin"), "Error")

    def test_items_count_greater_than_5_and_customer_with_tax_exempt(self):
        """
        11. Check item count greater than 5 less than 10, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids3, "Kevin"), "$950.00")

    def test_items_count_equal_10_and_customer_with_tax_exempt(self):
        """
        12. Check item count equal with 10, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids8, "Kevin"), "$900.00")

    def test_items_count_greater_than_10_and_customer_with_tax_exempt(self):
        """
        13. Check item count greater than 10 less than 50, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids8, "Kevin"), "$900.00")

    def test_items_count_less_than_5_and_customer_with_tax_exempt(self):
        """
        14. Check item count less than 5, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids4, "Kevin"), "$1000.00")

    def test_items_count_equal_with_5_and_customer_with_tax_exempt(self):
        """
        15. Check item count equal with  5, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids9, "Kevin"), "$1000.00")

    def test_items_count_equal_with_6_and_customer_with_tax_exempt(self):
        """
        16. Check item count equal with 6, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids10, "Kevin"), "$950.00")

    def test_pre_tax_down_and_customer_with_tax_exempt_1(self):
        """
        item price is 1.0125
        17. test pre_tax round down, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids11, "Kevin"), "$1.01")

    def test_pre_tax_up_and_customer_with_tax_exempt_1(self):
        """
        item price is 1.0625
        18. test pre_tax round up, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids12, "Kevin"), "$1.06")

    def test_pre_tax_up_and_customer_with_tax_exempt_2(self):
        """
        item price is 1.0250
        19. test pre_tax round up 2, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids13, "Kevin"), "$1.03")

    def test_pre_tax_down_and_customer_with_tax_exempt_2(self):
        """
        item price is 1.0249
        20. test pre_tax round down 2, customer with tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids14, "Kevin"), "$1.02")

    def test_post_tax_around_and_customer_without_tax_exempt_1(self):
        """
        item price is 1.6250
        21. test post_tax round, customer without tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids15, "eve"), "$1.70")

    def test_post_tax_around_and_customer_without_tax_exempt_2(self):
        """
        item price is 1.0000
        22. test post_tax round, customer without tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids17, "eve"), "$1.05")

    def test_post_tax_around_and_customer_without_tax_exempt_3(self):
        """
        item price is 0.9900
        23. test post_tax round, customer without tax_exempt not a club memeber
        """
        self.assertEqual(self.shopping_cart.calculatePurchasePrice(self.product_ids16, "eve"), "$1.03")

if __name__ == '__main__':
    # unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(TestShoppingCart)
    unittest.TextTestRunner(verbosity=2).run(suite)

